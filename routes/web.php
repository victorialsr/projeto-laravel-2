<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
})->name('site.home');

Route::get('/sobre', function () {
    return view('site.sobre');
})->name('site.sobre');

Route::get('/descontos', function () {
    return view('site.descontos');
})->name('site.descontos');

Route::get('/faq', function () {
    return view('site.faq');
})->name('site.faq');

Route::get('/parceiro', function () {
    return view('site.parceiro');
})->name('site.parceiro');

Route::get('/ajuda', function () {
    return view('site.ajuda');
})->name('site.ajuda');

Route::get('/blog', function () {
    return view('site.blog');
})->name('site.blog');

Route::get('/blog-integra', function () {
    return view('site.blog-integra');
})->name('site.blog-integra');

Route::get('/descontos-integra', function () {
    return view('site.descontos-integra');
})->name('site.descontos-integra');
