<div class="vantagens">
	<div class="sobre-main">
		<div class="container content-sobre-main">
			<img src="site/img/logo-clube.png" alt="">
			<div class="text-sobre-main">
				<h3>O Clube</h3>
				<p>O Clube é um programa de relacionamentos com os Assinantes A Tribuna que oferece diversos benefícios para os conveniados A Tribuna.</p>
				<p>Além de muita informação, os assinantes recebem condições exclusivas nas maiores empresas da região além de descontos em shows e espetáculos de teatro.</p>
			</div>
		</div>
	</div>
	<div class="sobre-vantagens container">
		<div class="content-sobre-vantagens">
			<div class="title-sobre-vantagens">	
					<img src="site/img/icon-check.svg" alt="">
					<h3><span>Vantagens</span> de ser do clube</h3>
			</div>
			<div class="onda"></div>
			<div class="texts-sobre-vantagens container">
				<div class="container-sobre-vantagens">
					<div class="text-sobre-vantagens">
						<img src="site/img/icon-calendar.svg" alt="">
						<h4>Acesso exclusivo a eventos</h4>
					</div>
					<p>Descontos nos principais eventos da região e ainda, promoções que vão desde sorteio de ingressos a experiências com artistas.</p>
				</div>
				<div class="container-sobre-vantagens">
					<div class="text-sobre-vantagens">
						<img src="site/img/icon-loudspeaker.svg" alt="">
						<h4>Informação toda a hora</h4>
					</div>
					<p>Acesso ao conteúdo do jornal A Tribuna na versão digital e física, mediante o plano de assinatura.</p>
				</div>
				<div class="container-sobre-vantagens">
					<div class="text-sobre-vantagens">
						<img src="site/img/icon-discount.svg" alt="">
						<h4>Plataforma Digital</h4>
					</div>
					<p>Descontos nos principais eventos da região e ainda, promoções que vão desde sorteio de ingressos a experiências com artistas.</p>
				</div>	
				<a href="" class="btn --assineSobre">Clique aqui e faça parte do clube</a>
			</div>
		</div>
		<img src="site/img/mulher-sobre.png" alt="">
	</div>
</div>