<section class="section-hero-component">
	<div class="container hero-component-content">
		<h1>@yield('title')</h1>
        <div class="breadcrumb">
		    <div class="container">
                <nav>
                    <ul class="container">
                        <li><a href="">Home</a></li>
                        <li>/</li>
                        <li><a href="">@yield('title')</a></li>
                    </ul>
                </nav>
		    </div>
	    </div>
    </div>
</section>