<div class="container">
<div class="swiper swiperEstabelecimentosConveniados">
      <div class="swiper-wrapper container">
        <div class="swiper-slide">
				<a href="#" class="a-main">
					<div class="img-container">
						<img src="site/img/img-conveniados-1.png" alt="">
						<div class="teste">
							<p>10%</p>
						</div>
					</div>
					<div class="content">
						<h3>Madê Cozinha autoral</h3>
						<p>Um restaurante simples, requintado e com muito sabore qualidade.</p>
						<p class="outro">+</p>
					</div>
				</a>
				</div>
        <div class="swiper-slide"><a href="#" class="a-main">
					<div class="img-container">
						<img src="site/img/img-conveniados-1.png" alt="">
						<div class="teste">
							<p>10%</p>
						</div>
					</div>
					<div class="content">
						<h3>Madê Cozinha autoral</h3>
						<p>Um restaurante simples, requintado e com muito sabore qualidade.</p>
						<p class="outro">+</p>
					</div>
				</a></div>
        <div class="swiper-slide"><a href="#" class="a-main">
					<div class="img-container">
						<img src="site/img/img-conveniados-1.png" alt="">
						<div class="teste">
							<p>10%</p>
						</div>
					</div>
					<div class="content">
						<h3>Madê Cozinha autoral</h3>
						<p>Um restaurante simples, requintado e com muito sabore qualidade.</p>
						<p class="outro">+</p>
					</div>
				</a>
			</div>
      </div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
      <div class="swiper-pagination"></div>
    </div>
</div>