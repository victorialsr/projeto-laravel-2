<div class="swiper swiperBlog container">
	<div class="swiper-wrapper">
		<div class="swiper-slide">
			<div class="card-blog">
				<a href="{{ route('site.blog-integra') }}" class="a-main">
					<div class="img-card-blog-container">
						<img src="site/img/img-blog-1.png" alt="">
					</div>	
					<div class="card-blog-content">
						<p class="card-blog-data">21/01/2020</p>
						<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
						<p class="btn" >+</p>
					</div>
				</a>
			</div>
		</div>
		<div class="swiper-slide">
			<div class="card-blog">
				<a href="{{ route('site.blog-integra') }}" class="a-main">
					<div class="img-card-blog-container">
						<img src="site/img/img-blog-1.png" alt="">
					</div>	
					<div class="card-blog-content">
						<p class="card-blog-data">21/01/2020</p>
						<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
						<p class="btn" >+</p>
					</div>
				</a>
			</div>
		</div>
		<div class="swiper-slide">
			<div class="card-blog">
				<a href="{{ route('site.blog-integra') }}" class="a-main">
					<div class="img-card-blog-container">
						<img src="site/img/img-blog-1.png" alt="">
					</div>	
					<div class="card-blog-content">
						<p class="card-blog-data">21/01/2020</p>
						<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
						<p class="btn" >+</p>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="swiper-pagination"></div>
</div>
