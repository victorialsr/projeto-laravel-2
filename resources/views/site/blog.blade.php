@extends('site.master.layout')

@section('title', 'Blog')

@section('content')
<x-hero></x-hero>
<section class="section-listagem-blog">
  <div class="container listagem-blog-content">
		<div class="title-wrapper">
			<h2 class="title">Acompanhe as notícias do clube</h2>
			<form action="">
				<div class="filter-wrapper">
					<img src="site/img/icon-filter.svg" alt="">	
					<label for="">O que você procura</label>		
					<input type="text">
					<button>Ok</button>
				</div>
				<div class="select-wrapper">
					<select name="" id="" placeholder="Categoria">
						<option value="">Categoria 1</option>
						<option value="">Categoria 1</option>
						<option value="">Categoria 1</option>
					</select>
				</div>
			</form>
		</div>
    <!-- <div class="listagem-blog">
    	<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
		</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
			<div class="card-blog">
        <a href="#" class="a-main">
          <div class="img-card-blog-container">
            <img src="site/img/img-blog-1.png" alt="">
          </div>	
          <div class="card-blog-content">
      			<p class="card-blog-data">21/01/2020</p>
          	<h3>6 dicas de jardinagem para ter plantas lindas e viçosas</h3>
          	<p class="btn" >+</p>
        	</div>
        </a>
			</div>
  	</div> -->
	<x-listagem-blog></x-listagem-blog>
  </div>
</section>


@endsection