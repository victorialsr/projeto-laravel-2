@extends('site.master.layout')

@section('title', 'Descontos')

@section('content')
<x-hero></x-hero>
<section class="section-listagem-estabelecimentos-conveniados">
  <div class="container estabelecimentos-conveniados-content">
	<div class="title-wrapper">
			<h2 class="title">Acompanhe as notícias do clube</h2>
			<form action="">
				<div class="filter-wrapper">
					<img src="site/img/icon-filter.svg" alt="">	
					<label for="">O que você procura</label>		
					<input type="text">
					<button>Ok</button>
				</div>
				<div class="select-wrapper">
					<select name="" id="" placeholder="Categoria">
						<option value="">Categoria 1</option>
						<option value="">Categoria 1</option>
						<option value="">Categoria 1</option>
					</select>
				</div>
			</form>
		</div>
	<x-listagem-estabelecimentos-conveniados></x-listagem-estabelecimentos-conveniados>
  </div>
</section>
	
@endsection
