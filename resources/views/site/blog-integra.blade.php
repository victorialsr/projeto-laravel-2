@extends('site.master.layout')

@section('title', 'Blog')


@section('content')
<x-hero></x-hero>
<section class="section-blog-integra">
    <div class="img-integra-wrapper">
        <img src="site/img/hero-blog-integra.png" alt="">
    </div>
    <div class="blog-integra-content">
        <div class="blog-post container ">
            <div class="blog-post-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <p>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. </p>
            </div>
            <div class="blog-share-content">
                <a href="#"> 
                    <img src="site/img/icon-facebook-footer.svg" alt="Ícone da rede social Facebook">
                </a>
                <a href="#"> 
                    <img src="site/img/icon-facebook-footer.svg" alt="Ícone da rede social Twitter">
                </a>
                <a href="#"> 
                    <img src="site/img/icon-facebook-footer.svg" alt="Ícone da empresa Google">
                </a>    
                <a href="#"> 
                    <img src="site/img/icon-facebook-footer.svg" alt="Ícone da rede social Linkedin">
                </a>                  
                <p>Compartilhe</p>
            </div>
        </div>
    </div>
    <div class="line-wrapper ">
        <div class="line container">    
        </div>
    </div>
    <div class="related-posts container">
        <div class="title-wrapper">
            <h3>Leia também</h3>
            <h2>Outras notícias</h2>
        </div>
        <x-listagem-blog></x-listagem-blog>
    </div>
</section>


@endsection