@extends('site.master.layout')

@section('title', 'Descontos')

@section('content')
<x-hero></x-hero>
<section class="section-estabelecimentos-conveniados-integra">
  <div class="container estabelecimentos-conveniados-integra-content">
		<div class="intro-row">
			<div class="company-wrapper">
				<div class="img-container">
					<img src="site/img/img-descontos-integra.png" alt="">
				</div>
				<div class="title-wrapper">
					<h2 class="title">China in Box</h2>
					<p>Culinária Chinesa conseituada e com sabor inesquecível.</p>
				</div>
			</div>
			<div class="discount-wrapper">
				<div class="discount-content">
					<span>15%</span>
					<p>de desconto</p>
				</div>
				<a href="">Voltar</a>
			</div>
		</div>
		<p class="description">
			O <strong>China in box</strong> é o lugar certo para quem procura a tradicional culinária chinesa, com um sabor incomparável. Os “carros chefes” do restaurante são o yakissoba, o frango xadres e delicioso rolinho primavera. Os pratos do China on box são preparados com rígidos controles de qualidade des de 1992 que faz da marca um sucesso.
		</p>
		<div class="gallery">
			<div class="swiper sliderDescontosIntegra">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
				</div>
				<div class="swiper-button-next"></div>
				<div class="swiper-button-prev"></div>
			</div>
			<div thumbsSlider="" class="swiper sliderDescontosIntegraThumb">
				<div class="swiper-wrapper">
				<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
					<div class="swiper-slide">
						<img src="site/img/descontos-integra-produto-slide.jpg" />
					</div>
				</div>
			</div>
		</div>
		<div class="information">
			<div class="usage">
				<h3>Regras de uso</h3>
				<p>15% de desconto* para os assinantes A tribuna atravéz do cupom do Clube</p>
				<p>* Desconto válido para compras acima de R$30,00 no delivery ou com retirada na loja e não cumulativo com outras promoções. Frete não incluso no desconto</p>
				<a href="">Para acessar o cupom clique aqui</a>
			</div>
			<div class="contact-information">
				<div class="card-information">
					<div class="card-location">
						<h4>Onde estamos</h4>
						<address>Rua Dr. Tolentino Filgueiras, 54 - Gonzaga, Santos - SP, 11060-470</address>
					</div>
					<div class="card-contact">
						<h4>Fale conosco</h4>
						<div class="telephone-wrapper">
							<img src="" alt="">
							<p>(13) 3289-5060</p>
						</div>
						<div class="telephone-wrapper">
							<img src="" alt="">
							<p>(13) 3289-5060</p>
						</div>
						<div class="email-wrapper">
							<img src="" alt="">
							<p>chinainboxgonzaga.com.br</p>
						</div>
					</div>
					<div class="card-socialmedia">
						<div class="icon-img-container">
							<img src="site/img/icon-facebook-footer.svg" alt="">
						</div>
						<div class="icon-img-container">
							<img src="site/img/icon-instagram-footer.svg" alt="">
						</div>
						<p>#acompanhe-nos</p>	
					</div>	
				</div>
			</div>
		</div>
  </div>
</section>
	
@endsection
