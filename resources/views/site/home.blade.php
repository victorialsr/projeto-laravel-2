@extends('site.master.layout')

@section('content')
<section class="section-hero">
	<div class="descontos-home">
		<div class="descontos container">
			<h3>O maior clube de vantagens da região</h3>
			<h2>Com o que você gasta no mês?</h2>
			<div class="descontos-wraper">
				<div class="desconto-wraper">
					<img src="site/img/icon-restaurant.svg" alt="">
					<div class="desconto-number">
						<span>R$ 100</span>
						<span>20%</span>
						<span>= R$ 20</span>
					</div>
					<div class="desconto-text">
						<span>Restaurante</span>
						<span>Desconto do clube</span>
						<span>De economia</span>
					</div>
				</div>
				<div class="desconto-wraper">
					<img src="site/img/icon-gaspump.svg" alt="">
					<div class="desconto-number">
						<span>R$200,00</span>
						<span>5%</span>
						<span>=  R$ 10</span>
					</div>
					<div class="desconto-text">
						<span>Gasolina</span>
						<span>Desconto do clube</span>
						<span>De economia</span>
					</div>
				</div>
				<div class="desconto-wraper">
					<img src="site/img/icon-washing-machine.svg" alt="">
					<div class="desconto-number">
						<span>R$ 100</span>
						<span>20%</span>
						<span>= R$ 20</span>
					</div>
					<div class="desconto-text">
						<span>Lavanderia</span>
						<span>Desconto do clube</span>
						<span>De economia</span>
					</div>
				</div>
			</div>
			<a href="" class="btn --clubeHome">Clique aqui e faça parte do clube</a>
			<p>Sua economia pode pagar a sua assinatura</p>
		</div>
	</div>
</section>

<section class="section-descontos">	
	<div class="text-descontos">
		<div class="container text-desconto-container">
			<img src="site/img/icon-coupon.svg" alt="">
			<div class="text-container">	
				<h3>Tenha acesso a</h3>
				<span>Descontos exclusivos</span>
			</div>
		</div>
	</div>
	<div class="teste-black">
		<div class="col col-black">	
		
		</div>
		<div class="col"></div>
		<div class="col"></div>
		<div class="col"></div>
		
	</div>
	<div class="teste">
		<div class="descontos-main container">
		<div class="cards-descontos">
			<div class="card-descontos left">
				<img src="site/img/icon-serve.svg" alt="">
				<p>Gastronomia</p>
			</div>
			<div class="card-descontos left">
			<img src="site/img/icon-flower.svg" alt="">
				<p>Casa e decoração</p>
			</div>
			<div class="card-descontos left">
			<img src="site/img/icon-popcorn.svg" alt="">
				<p>Entretenimento</p>
			</div>
		</div>
		<div class="img-wraper">
			<img src="site/img/img-phone.png" alt="">
			<a href="" class="btn --clubeDescontos">Faça parte do clube</a>
		</div>
		<div class="cards-descontos">
			<div class="card-descontos right">
				<img src="site/img/icon-fashion.svg" alt="">
				<p>Moda e beleza</p>
			</div>
			<div class="card-descontos right">
			<img src="site/img/icon-heart.svg" alt="">
				<p>Saúde e bem estar</p>
			</div>
			<div class="card-descontos right">
			<img src="site/img/icon-technical-support.svg" alt="">
				<p>Serviços</p>
			</div>
		</div>
	</div>

</section>

<section>
	<x-slider-estabelecimentos-conveniados>
	</x-slider-estabelecimentos-conveniados>
</section>

<section class="section-vantagens">
	<x-vantagens></x-vantagens>
	<x-slider-blog></x-slider-blog>
</section>

<section class="section-economia">
	<div class="container economia-content">
		<div class="col-1">
			<div class="title">
				<img src="site/img/icon-calculator.svg" alt="">
				<h3>Calculadora de Economia <span>Clube A Tribuna</span></h3>
			</div>
			<div class="content">
				<h2>Saiba quanto você pode economizar em seu dia-a-dia fazendo parte do Clube A Tribuna!</h2>
				<p>Os valores apresentados tratam-se de estimativas, com base na porcentagem de desconto oferecida aos <span>assinantes do Clube A Tribuna.</span> </p>
				<a href="" class="btn --assineEconomia">Assine agora!</a>
			</div>
		</div>
		<div class="col-2">
			<div class="calculadora-economia">
				<div class="row-1">
					<p>Você economizará:</p>
					<h3 id="calculadora-resultado">R$</h3>
				</div>
				<div class="row-2">
					<p>Durante um mês, qual o valor que você gosta com:</p>
				</div>
				<div class="row-3">
					<div class="calculadora-economia-categoria">
						<img src="site/img/icon-fork.svg" alt="">
						<h3>Idas ao restaurante?</h3>
						<div class="input-teste">
							<input type="number" id="calc_restaurante" onBlur="calcular()">
							<p>R$</p>
						</div>
					</div>
					<div class="calculadora-economia-categoria">
						<img src="site/img/icon-gas-station.svg" alt="">
						<h3>Abastecimento de veículos com gasolina?</h3>
						<div class="input-teste">
							<input type="number" id="calc_gasolina" onBlur="calcular()">
							<p>R$</p>
						</div>
					</div>
					<div class="calculadora-economia-categoria">
						<img src="site/img/icon-washing-machine-1.svg" alt="">
						<h3>Utilização de serviços de lavanderia?</h3>
						<div class="input-teste">
							<input type="number" id="calc_lavanderia" onBlur="calcular()">
							<p>R$</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-slider-descontos">
	<div class="container">
		<div class="slider-descontos-content">
			<div class="text">
				<h3>Grandes empresas</h3>
				<h2>Grandes descontos</h2>
				<p>Centenas de parceiros para você economizar muito.</p>
			</div>
			<x-slider-descontos></x-slider-descontos>
		</div>
	</div>
</section>

@endsection
