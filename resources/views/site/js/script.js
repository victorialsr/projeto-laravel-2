var swiper = new Swiper(".swiperDescontos", {
    slidesPerView: 3,
    grid: {
        rows: 2,
    },
    spaceBetween: 30,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

var swiper = new Swiper(".swiperEstabelecimentosConveniados", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        800: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        1000: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    },
});
var swiper = new Swiper(".swiperBlog", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        500: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        1000: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    },
});

var swiper = new Swiper(".sliderDescontosIntegraThumb", {
    loop: true,
    spaceBetween: 15,
    slidesPerView: 6,
    freeMode: true,
    watchSlidesProgress: true,
});
var swiper2 = new Swiper(".sliderDescontosIntegra", {
    loop: true,
    spaceBetween: 10,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    thumbs: {
        swiper: swiper,
    },
});

/*hamburguer menu*/
const mobileBtn = document.querySelector("#btn-mobile");
const navLinksToggle = document.querySelectorAll(".menu-link a");

function toggleMenu() {
    const nav = document.querySelector(".nav-menu");
    nav.classList.toggle("active");
}
function toggleLink() {
    navLinksToggle.forEach((navLink) => {
        navLink.addEventListener("click", toggleMenu);
    });
}
mobileBtn.addEventListener("click", toggleMenu);
toggleLink();
/*hamburguer menu*/

function calcular() {
    var num1 = Number(document.getElementById("calc_restaurante").value);
    var num2 = Number(document.getElementById("calc_gasolina").value);
    var num3 = Number(document.getElementById("calc_lavanderia").value);
    var elemResult = document.getElementById("calculadora-resultado");
    num1 = num1 * 0.2;
    num2 = num2 * 0.05;
    num3 = num3 * 0.2;
    elemResult.innerText = "R$" + String(num1 + num2 + num3);
}
